+++
# Display name
name = "Michael Pereira"

# Username (this should match the folder name)
authors = ["admin"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "SNSF Postdoc Mobility fellow"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "CNRS", url = "" } ]

# Short bio (displayed in user profile at end of posts)
bio = "I am a postdoctoral researcher in cognitive neurosciences studying the neural mechanisms of perceptual consciousness and metacognition with psychophysics and computational modelling in healthy individuals as well as invasive electrophysiology with psychiatric and neurological patients. My goal is to find a mechanistic explanation of how single neurons in the human brain explains the fluctuations of our subjective perceptual experience within a single trial. I am currently hosted at the [*Laboratoire de Psychologie et NeuroCognition*] (http://lpnc.univ-grenoble-alpes.fr) by [*Dr. Nathan Faivre*] (https://nfaivre.netlify.app), funded by the [*Swiss National Science Foundation*] (http://www.snf.ch). "


# Enter email to display Gravatar (if Gravatar enabled in Config)
email = ""

# List (academic) interests or hobbies
interests = [
  "Decision-making",
  "Perceptual consciousness",
  "Metacognition",
  "Evidence accumulation",
  "Human single neuron recordings",
  "Computational modelling"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Researchers", "Visitors"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "PhD in Electrical Engineering"
  institution = "EPFL"
  year = 2017

[[education.courses]]
  course = "MEng in Communication Systems"
  institution = "EPFL"
  year = 2010

[[education.courses]]
  course = "BSc in Communication Systems"
  institution = "EPFL"
  year = 2007

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "twitter"
  icon_pack = "fab"
  link = "https://twitter.com/meaperei"

[[social]]
  icon = "google-scholar"
  icon_pack = "ai"
  link = "https://scholar.google.com/citations?user=1v1RJ44AAAAJ&hl=en"

[[social]]
  icon = "github"
  icon_pack = "fab"
  link = "https://gitlab.com/michael.pereira"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
 [[social]]
   icon = "cv"
   icon_pack = "ai"
   link = "files/cv.pdf"

+++

I am a postdoctoral researcher in cognitive neurosciences studying the neural mechanisms of perceptual consciousness and metacognition with psychophysics and computational modelling in healthy individuals as well as invasive electrophysiology with psychiatric and neurological patients. My goal is to find a mechanistic explanation of how the activity of single neurons in the human brain explains the fluctuations of our subjective perceptual experience within a single trial. 

I am currently hosted at the [*Laboratoire de Psychologie et NeuroCognition*] (http://lpnc.univ-grenoble-alpes.fr) by [*Dr. Nathan Faivre*] (https://nfaivre.netlify.app), funded by the [*Swiss National Science Foundation*] (http://www.snf.ch). 

