+++
# Experience widget.
widget = "experience"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Experience"
subtitle = ""

# Order that this section will appear in.
weight = 8

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Postdoc"
  company = "LPNC (CNRS) - Postdoc Mobility from the [*SNSF*] (http://www.snf.ch)"
  company_url = "http://nfaivre.netlify.com"
  location = "Grenoble, France"
  date_start = "2021-05-01"
  date_end = ""
  description = """
  Unravelling the mechanisms underlying perceptual consciousness with Nathan Faivre, funded by the Swiss National Science Foundation [*P400PM_199251*](https://p3.snf.ch/project-199251).
  """

[[experience]]
  title = "Postdoc"
  company = "LPNC (CNRS) - Early.Postdoc Mobility from the [*SNSF*] (http://www.snf.ch)"
  company_url = "http://nfaivre.netlify.com"
  location = "Grenoble, France"
  date_start = "2019-11-01"
  date_end = "2021-04-30"
  description = """
  Using deep brain stimulation to dig out the subcortical correlates of consciousness with Nathan Faivre, funded by the Swiss National Science Foundation [*P2ELP3_187974*](https://p3.snf.ch/project-187974).
  """

[[experience]]
  title = "Postdoc"
  company = "LNCO (EPFL, CH) / RNI (WVU, US)"
  company_url = "http://lnco.epfl.ch"
  location = "Geneva, Switzerland / Morgantown, United States"
  date_start = "2018-03-01"
  date_end = "2019-10-31"
  description = """
  Studying neural correlates of perceptual consciousness and sensory attenuation in the laboratory of Olaf Blanke and at the Rockefeller Neuroscience Institute with Ali Rezai.
  """

[[experience]]
  title = "PhD student"
  company = "CNBI (EPFL, CH)"
  company_url = "http://cnbi.epfl.ch"
  location = "Geneva, Switzerland"
  date_start = "2012-11-02"
  date_end = "2017-12-31"
  description = """
  Studying action monitoring in continuous motor tasks in the laboratory of José del R. Millán. Defended on the 7th of November 2017. Research visits at UC Berkeley, US with [*Robert T. Knight*](https://knightlab.neuro.berkeley.edu/) and at INRIA, FR with [*Anatole Lecuyer*](https://team.inria.fr/hybrid/).
  """

[[experience]]
  title = "Research engineer"
  company = "Sony Deutschland GmbH, DE"
  company_url = ""
  location = "Stuttgart, Germany"
  date_start = "2010-04-01"
  date_end = "2012-03-31"
  description = """
  Signal processing and machine learning for speech and music.
  """
+++
