+++
title = "Action Monitoring Cortical Activity Coupled to Submovements"
date = 2017-10-13T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Michael Pereira", "Aleksander Sobolewski","José del R. Millàn"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *eNeuro*"
publication_short = "In *eNeuro*"

# Abstract.
abstract = "Numerous studies have examined neural correlates of the human brain’s action-monitoring system during experimentally segmented tasks. However, it remains unknown how such a system operates during continuous motor output when no experimental time marker is available (such as button presses or stimulus onset). We set out to investigate the electrophysiological correlates of action monitoring when hand position has to be repeatedly monitored and corrected. For this, we recorded high-density electroencephalography (EEG) during a visuomotor tracking task during which participants had to follow a target with the mouse cursor along a visible trajectory. By decomposing hand kinematics into naturally occurring periodic submovements, we found an event-related potential (ERP) time-locked to these submovements and localized in a sensorimotor cortical network comprising the supplementary motor area (SMA) and the precentral gyrus. Critically, the amplitude of the ERP correlated with the deviation of the cursor, 110 ms before the submovement. Control analyses showed that this correlation was truly due to the cursor deviation and not to differences in submovement kinematics or to the visual content of the task. The ERP closely resembled those found in response to mismatch events in typical cognitive neuroscience experiments. Our results demonstrate the existence of a cortical process in the SMA, evaluating hand position in synchrony with submovements. These findings suggest a functional role of submovements in a sensorimotor loop of periodic monitoring and correction and generalize previous results from the field of action monitoring to cases where action has to be repeatedly monitored."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1523/ENEURO.0241-17.2017"

# Is this a featured publication? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["EEG","error","kinematics","monitoring","submovements","supplementary motor area"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["visuomotor"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = "http://www.eneuro.org/content/eneuro/4/5/ENEURO.0241-17.2017.full.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

