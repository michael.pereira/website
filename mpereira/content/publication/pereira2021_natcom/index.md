+++
title = "Evidence accumulation relates to perceptual consciousness and monitoring"
date = 2021-05-31T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Michael Pereira", "Pierre Megevand","Mi Xue Tan","Wenwen Chang","Shuo Wang","Ali Rezai","Margitta Seeck","Marco Corniola","Shahan Momjian","Fosco Bernasconi","Olaf Blanke", "Nathan Faivre"]
# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *Nature Communications*"
publication_short = "In *Nature Communications*"

# Abstract.
abstract = "A fundamental scientific question concerns the neural basis of perceptual consciousness and perceptual monitoring resulting from the processing of sensory events. Although recent studies identified neurons reflecting stimulus visibility, their functional role remains unknown. Here, we show that perceptual consciousness and monitoring involve evidence accumulation. We recorded single-neuron activity in a participant with a microelectrode in the posterior parietal cortex, while they detected vibrotactile stimuli around detection threshold and provided confidence estimates. We find that detected stimuli elicited neuronal responses resembling evidence accumulation during decision-making, irrespective of motor confounds or task demands. We generalize these findings in healthy volunteers using electroencephalography. Behavioral and neural responses are reproduced with a computational model considering a stimulus as detected if accumulated evidence reaches a bound, and confidence as the distance between maximal evidence and that bound. We conclude that gradual changes in neuronal dynamics during evidence accumulation relates to perceptual consciousness and perceptual monitoring in humans."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1038/s41467-021-23540-y"

# Is this a featured publication? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["tactile perception","consciousness","confidence","metacognition","Utah array","single-unit activity","evidence","accumulation","neuron"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["consciousness"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = "https://www.nature.com/articles/s41467-021-23540-y.pdf"
url_preprint = "https://www.biorxiv.org/content/10.1101/2020.07.10.196659v1"
url_code = "https://doi.org/10.17605/OSF.IO/YHXDB"
url_dataset = "https://doi.org/10.18112/openneuro.ds001785.v1.1.1"
url_project = ""
url_slides = ""
url_video = "https://www.youtube.com/watch?v=ufhwzKZLunk"
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

