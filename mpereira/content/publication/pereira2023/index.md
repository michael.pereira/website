+++
title = "Subcortical correlates of consciousness with human single neuron recordings"
date = 2023-01-27T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Michael Pereira", "Nathan Faivre", "Fosco Bernasconi","Nicholas Brandmeir","Jacob Suffridge","Kaylee Tran","Shuo Wang","Victor Finomore","Peter Konrad","Ali Rezai","Olaf Blanke"]
# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = "In *Biorxiv*"
publication_short = "In *Biorxiv*"

# Abstract.
abstract = "How does neuronal activity give rise to our conscious experience of the outside world? This question has fascinated philosophers for centuries and is being increasingly addressed empirically. Current methods to investigate the neural correlates of consciousness aim at contrasting the neural activity associated with different percepts under constant sensory stimulation to identify the minimal set of neuronal events sufficient for a specific conscious percept to occur (1–3). Only very few studies have found such contrasts at the single neuron level (4–8) but did so only in cortical regions of humans capable of providing subjective reports. The role of subcortical structures for perceptual consciousness is theoretically relevant (2,9,10) with some empirical support from studies in non-human primates (11,12), as well as functional imaging or local field potentials in humans (13,14). Nonetheless, it remains unknown whether and how the firing rate of subcortical neurons changes when a stimulus is consciously perceived. Here, we recorded individual neurons from the subthalamic nucleus (STN) and thalamus of human participants during 36 deep brain stimulation surgeries. While participants detected vibrotactile stimuli provided at the perceptual threshold, we found that neurons in both subcortical structures were modulated by the onset of the task or of the stimulus. Importantly, we found that 23% of the recorded neurons changed their activity when a stimulus was consciously perceived. Our results provide direct neurophysiological evidence of the involvement of subcortical structures in perceptual consciousness, thereby calling for a less cortico-centric view of the neural correlates of consciousness."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1101/2023.01.27.525684"

# Is this a featured publication? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["deep brain stimulation","single neurons","consciousness","detection"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["consciousness"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = "https://mpereira.netlify.com/publication/pereira2023/pereira2023.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

