+++
title = "Prior information differentially affects discrimination decisions and subjective confidence reports"
date = 2022-10-27T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Constant Marika","Michael Pereira", "Nathan Faivre", "Filevich Elisa"]
# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = "In *Biorxiv*"
publication_short = "In *Biorxiv*"

# Abstract.
abstract = "According to Bayesian models, both decisions and confidence are based on the same precision-weighted integration of prior expectations (“priors”) and incoming information (“likelihoods”). This assumes that priors are integrated optimally and equally in decisions and confidence, which has not been tested. In two experiments, we quantitatively assessed how priors inform both decisions and confidence. With a gamified dual-decision task we controlled the strength of priors and likelihoods to create pairs of conditions that were matched in posterior information, but differed on whether the prior or likelihood was more informative. We found that priors were underweighted in discrimination decisions, but used to a greater extent in confidence about those decisions, and this was not due to differences in processing time. With a Bayesian model we quantified the weighting parameters for the prior at both levels, and confirmed that priors are more optimally used in explicit confidence, even when underused in decisions."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1101/2022.10.26.513829"

# Is this a featured publication? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["confidence","metacognition","prior"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["consciousness"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = "https://mpereira.netlify.com/publication/constant2022/constant2022.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

