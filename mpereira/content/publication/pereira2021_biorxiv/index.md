+++
title = "Optimal confidence for unaware visuomotor deviations"
date = 2021-10-24T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Michael Pereira", "Rafal Skiba", "Yann Cojan", "Patrik Vuilleumier", "Indrit Begue"]
# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = "In *Biorxiv*"
publication_short = "In *Biorxiv*"

# Abstract.
abstract = "Numerous studies have shown that humans can successfully correct deviations to ongoing movements without being aware of them, suggesting limited conscious monitoring of visuomotor performance. Here, we ask whether such limited monitoring impairs the capacity to judiciously place confidence ratings to reflect decision accuracy (metacognitive sensitivity). To this end, we recorded functional magnetic resonance imaging data while thirty-one participants reported visuomotor cursor deviations and rated their confidence retrospectively. We show that participants use a summary statistic of the unfolding visual feedback (the maximum cursor error) to detect deviations but that this information alone is insufficient to explain detection performance. The same summary statistics is used by participants to optimally adjust their confidence ratings, even for unaware deviations. At the neural level, activity in the ventral striatum tracked high confidence, whereas a broad network including the anterior prefrontal cortex encoded cursor error but not confidence, shedding new light on a role of the anterior prefrontal cortex for action monitoring rather than confidence. Together, our results challenge the notion of limited action monitoring and uncover a new mechanism by which humans optimally monitor their movements as they unfold, even when unaware of ongoing deviations."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1101/2021.10.22.465492"

# Is this a featured publication? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["consciousness","confidence","metacognition","visuomotor","monitoring"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["visuomotor","consciousness"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_preprint = "https://www.biorxiv.org/content/10.1101/2021.10.22.465492v1.full.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

