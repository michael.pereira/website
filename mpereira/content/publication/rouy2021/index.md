+++
title = "Systematic review and meta-analysis of metacognitive abilities in individuals with schizophrenia spectrum disorders"
date = 2021-07-01T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Rouy Martin","Pauline Saliou", "Ladislas Nalborczyk", "Michael Pereira", "Paul Roux", "Nathan Faivre"]
# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *Neuroscience & Biobehavioral Reviews*"
publication_short = "In *J.Neubiorev.*"

# Abstract.
abstract = "Metacognitive deficits are well documented in schizophrenia spectrum disorders as a decreased capacity to adjust confidence to performance in a cognitive task. Because metacognitive ability directly depends on task performance, metacognitive deficits might be driven by lower task performance among patients. To test this hypothesis, we conducted a Bayesian meta-analysis of 42 studies comparing metacognitive abilities in 1425 individuals with schizophrenia compared to 1256 matched controls. We found a global metacognitive deficit in schizophrenia (g = −0.57, 95 % CrI [−0.72, −0.43]), which was driven by studies which did not control task performance (g = −0.63, 95 % CrI [−0.78, −0.49]), and inconclusive among controlled-studies (g = −0.23, 95 % CrI [−0.60, 0.16], BF01 = 2.2). No correlation was found between metacognitive deficit and clinical features. We provide evidence that the metacognitive deficit in schizophrenia is inflated due to non-equated task performance. Thus, efforts should be made to develop experimental protocols accounting for lower task performance in schizophrenia."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1016/j.neubiorev.2021.03.017"

# Is this a featured publication? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["confidence","metacognition","meta","analysis","schizophrenia"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = "rouy2021.pdf"
url_preprint = ""
url_code = "https://gitlab.com/nfaivre/meta_analysis_scz_public"
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

