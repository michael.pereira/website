+++
title = "Confidence in perceptual decision-making is preserved in schizophrenia"

date = 2020-08-01T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Nathan Faivre*","Matthieu Roger*","Michael Pereira", "Vincent de Gardelle","Jean-Christophe Vergnaud","Christine Passerieux","Paul Roux"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *Journal of Psychiatry and Neuroscience*"
publication_short = "In *Journal of Psychiatry and Neuroscience*"

# Abstract.
abstract = "Metacognition is the set of reflexive processes allowing humans to evaluate the accuracy of their mental operations. Deficits in synthetic metacognition have been described in schizophrenia using mostly narrative assessment and linked to several key symptoms. Here, we assessed metacognitive performance by asking individuals with schizophrenia or schizoaffective disorder (N=20) and matched healthy participants (N = 21) to perform a visual discrimination task and subsequently report confidence in their performance. Metacognitive performance was defined as the adequacy between visual discrimination performance and confidence. Bayesian analyses revealed equivalent metacognitive performance in the two groups despite a weaker association between confidence and trajectory tracking during task execution among patients. These results were reproduced using a bounded evidence accumulation model which showed similar decisional processes in the two groups. The inability to accurately attune confidence to perceptual decisions in schizophrenia remains to be experimentally demonstrated, along with the way such impairments may underpin functional deficits."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1101/2019.12.15.19014969"
# Is this a featured publication? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []
# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["consciousness"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_preprint = "https://www.medrxiv.org/content/10.1101/2019.12.15.19014969v1.full.pdf"
url_code = "https://osf.io/84wqp/​"
url_dataset = "https://osf.io/84wqp/​"
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

