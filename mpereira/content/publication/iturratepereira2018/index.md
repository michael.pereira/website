+++
title = "Closed-loop electrical neurostimulation : Challenges and opportunities"
date = 2018-09-27T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Iñaki Iturrate","Michael Pereira", "José del R. Millàn"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *Current Opinion in Biomedical Engineering*"
publication_short = "In *Curr. Op. Bio. Eng.*"

# Abstract.
abstract = "Non-invasive and invasive electrical neurostimulation are promising tools to better understand brain functionandultimately treat its malfunction. In current open-loop approaches, a clinician chooses a fixed set of stimulation parameters, informed by observed therapeutic benefits and previous empirical evidence. However, this procedure leads to a large intra- and inter-subject variability often introducing side-effects and low effect sizes. Closed-loop electrical neurostimulation (CLENS) approaches strive to alleviate these limitations by tailoring the stimulation parameters to an ongoing electrophysiological biomarker. Here, we review the current status of closed-loop, supraspinal elec- trical stimulation in humans, presenting our vision of potential control frameworks, and support the idea of creating synergies with the field of brain-machine interfacing. Finally, we pinpoint two pivotal challenges that, in our view, need to be overcome for this technology to become a reality: dealing with the electrical stimulation artifacts, and dissociating the pathological from physiological information within the targeted biomarker."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1016/j.cobme.2018.09.007 2468-4511/©"

# Is this a featured publication? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Closed-loop electrical neurostimulation", "Deep brain stimulation", "Transcranial alternating current stimulation", "Transcranial direct current stimulation", "Electroencephalography", "Local field potentials"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["bci"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_preprint = "iturrate2018.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

