+++
title = "Sensory threshold neuromuscular electrical stimulation fosters motor imagery performance"
date = 2018-08-01T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Tiffany Corbet","Iñaki Iturrate","Michael Pereira", "Serafeim Perdikis","José del R. Millàn"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *Neuroimage*"
publication_short = "In *Neuroimage*"

# Abstract.
abstract = "Motor imagery (MI) has been largely studied as a way to enhance motor learning and to restore motor functions. Although it is agreed that users should emphasize kinesthetic imagery during MI, recordings of MI brain patterns are not sufficiently reliable for many subjects. It has been suggested that the usage of somatosensory feedback would be more suitable than standardly used visual feedback to enhance MI brain patterns. However, somatosensory feedback should not interfere with the recorded MI brain pattern. In this study we propose a novel feedback modality to guide subjects during MI based on sensory threshold neuromuscular electrical stimulation (St-NMES). St-NMES depolarizes sensory and motor axons without eliciting any muscular contraction. We hypothesize that St-NMES does not induce detectable ERD brain patterns and fosters MI performance. Twelve novice subjects were included in a cross-over design study. We recorded their EEG, comparing St-NMES with visual feedback during MI or resting tasks. We found that St-NMES not only induced significantly larger desynchronization over sensorimotor areas (p<0.05) but also significantly enhanced MI brain connectivity patterns. Moreover, classification accuracy and stability were significantly higher with St-NMES. Importantly, St-NMES alone did not induce detectable artifacts, but rather the changes in the detected patterns were due to an increased MI performance. Our findings indicate that St-NMES is a promising feedback in order to foster MI performance and cold be used for BMI online applications."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1016/j.neuroimage.2018.04.005"
# Is this a featured publication? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["EEG","NMES","BCI","St-NMES"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["bci"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_preprint = "corbet2018.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

