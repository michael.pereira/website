+++
title = "Disentangling the origins of confidence in speeded perceptual judgments through multimodal imaging"
date = 2020-04-01T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Michael Pereira", "Nathan Faivre", "Iñaki Iturrate", "Marco Wirthlin", "Luana Serafini", "Stéphanie Martin", "Arnaud Desvachez", "Olaf Blanke", "Dimitri Van De Ville", "José del R. Millàn"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *Proceedings of the National Academy of Science*"
publication_short = "In *Proceedings of the National Academy of Science*"

# Abstract.
abstract = "The human capacity to compute the likelihood that a decision is correct - known as metacognition - has proven difficult to study in isolation as it usually co-occurs with decision-making. Here, we isolated post-decisional from decisional contributions to metacognition by combining a novel paradigm with multimodal imaging. Healthy volunteers reported their confidence in the accuracy of decisions they made or decisions they observed. We found better metacognitive performance for committed vs. observed decisions, indicating that committing to a decision informs confidence. Relying on concurrent electroencephalography and hemodynamic recordings, we found a common correlate of confidence following committed and observed decisions in the inferior frontal gyrus, and a dissociation in the anterior prefrontal cortex and anterior insula. We discuss these results in light of decisional and post-decisional accounts of confidence, and propose a generative model of confidence in which metacognitive performance naturally improves when evidence accumulation is constrained upon committing a decision."

# Summary. An optional shortened abstract.
summary = "Our everyday life is full of rapid decisions such as a driver having to choose which exit to take on the highway. These rapid decisions often come with a certain level of subjective confidence ranging from certainty of having made the right choice to certainty of having chosen wrong and passing through various levels of uncertainty. Since our sense of confidence stems from the monitoring of the decision it relates to, its underlying brain mechanisms have been difficult to study in isolation of the accompanying decisions. We have published a study in which we isolate confidence from decisional processes by comparing confidence related to our own decisions with confidence related to other people’s decisions (e.g. the confidence of the passenger in the car). In our study, decisions were taken about which stimulus contained the highest number of dots. These decisions could be taken either by the study participants by pressing a button, or by the computer by showing a hand on the chosen side. In both cases, participants had to rate their confidence in the previous decision. We found that confidence ratings tracked the correctness of decisions better when those decisions were taken by the participants. Since we recorded electroencephalography while the participants were in a functional magnetic resonance imaging scanner, we were able to precisely locate the brain regions associated to confidence both in time and space. We found only one brain region that was still associated with confidence when participants rated the computer’s decisions: the inferior frontal cortex. One other brain region, the anterior prefrontal cortex, that is well known to relate to confidence was only associated to confidence in participants’ own decisions. Our study thus sheds light on the underlying mechanisms of confidence, highlighting the role of the inferior frontal cortex as a key region for confidence independently from decisions and constraining the role of the anterior prefrontal cortex to self-related monitoring."

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1073/pnas.1918335117"

# Is this a featured publication? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["metacognition", "error monitoring", "confidence", "EEG", "fMRI", "race modeling", "inferior frontal gyrus", "insula", "anterior prefrontal cortex"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["consciousness"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = "https://mpereira.netlify.com/publication/pereira2020_pnas/pereira2020_accepted.pdf"
url_preprint = "https://www.biorxiv.org/content/10.1101/496877v1"
url_code = "https://gitlab.com/nfaivre/analysis_public"
url_dataset = "https://openneuro.org/datasets/ds002158"
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

