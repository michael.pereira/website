+++
title = "A leaky evidence accumulation process for perceptual experience"
date = 2022-04-02T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Michael Pereira", "Denis Perrin", "Nathan Faivre"]
# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *Trends in Cognitive Sciences*"
publication_short = "In *Trends in Cognitive Sciences*"

# Abstract.
abstract = "The neural correlates supporting our perceptual experience of the world remain largely unknown. Recent studies have shown how stimulus detection and related confidence involve evidence accumulation (EA) processes similar to those involved in perceptual decision-making. Here, we propose that independently from any tasks, percepts are not static but fade in and out of consciousness according to the dynamics of a leaky evidence accumulation process (LEAP), and that confidence corresponds to the maximal evidence accumulated by this process. We discuss the implications and limitations of our proposal, assess how it may qualify as a neural correlate of consciousness, and illustrate how it brings us closer to a mechanistic understanding of phenomenal aspects of perceptual experience like intensity and duration, beyond mere detection."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1016/j.tics.2022.03.003"

# Is this a featured publication? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["consciousness","confidence","metacognition","evidence","accumulation","experience"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["consciousness"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = "https://mpereira.netlify.com/publication/pereira2022_tics/leap_tics.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

