+++
title = "Modulation of the inter-hemispheric asymmetry of motor-related brain activity using brain-computer interfaces"
date = 2015-11-05T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Michael Pereira", "Aleksander Sobolewski", "José del R. Millàn"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["1"]

# Publication name and optional abbreviated version.
publication = "In *37th Annual International Conference of the IEEE Engineering in Medicine and Biology Society (EMBC)*"
publication_short = "In *EMBC2015*"

# Abstract.
abstract = "Non-invasive brain stimulation has shown promising results in neurorehabilitation for motor-impaired stroke patients, by rebalancing the relative involvement of each hemisphere in movement generation. Similarly, brain-computer interfaces have been used to successfully facilitate movement-related brain activity spared by the infarct. We propose to merge both approaches by using BCI to train stroke patients to rebalance their motor-related brain activity during motor tasks, through the use of online feedback. In this pilot study, we report results showing that some healthy subjects were able to learn to spontaneously up- and/or down-regulate their ipsilateral brain activity during a single session."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "10.1109/EMBC.2015.7318857"

# Is this a featured publication? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["closed-loop decoding","brain-machine interface","brain-computer interface","mu","beta"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["bci"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_preprint = "pereira2015.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

