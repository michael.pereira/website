+++
title = "Preserved electrophysiological markers of confidence in schizophrenia spectrum disorder"
date = 2022-10-28T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Rouy Martin","Roger Matthieu", "Dorian Goueytes", "Michael Pereira", "Paul Roux", "Nathan Faivre"]
# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = "In *Biorxiv*"
publication_short = "In *Biorxiv*"

# Abstract.
abstract = "A large number of behavioral studies suggest that confidence judgments are impaired in schizophrenia, motivating the search for neural correlates of an underlying metacognitive impairment. Electrophysiological studies suggested that a specific evoked response potential reflecting performance monitoring, namely the error-related negativity (ERN), was blunted in schizophrenia compared to healthy controls. However, attention has recently been drawn to a potential confound in the study of metacognition, namely that lower first-order performance in schizophrenia compared to healthy controls could lead to a spurious decrease in metacognitive performance among patients. Here, we assessed how this confound might also apply to ERN-blunting in schizophrenia. We used an adaptive staircase procedure to titrate first-order performance on a motion discrimination task in which participants (N = 14 patients and 19 controls) had to report their confidence after each trial while we recorded high density EEG. Interestingly, not only metaperceptual abilities were preserved among patients at the behavioral level, but we also found no electrophysiological evidence for altered EEG markers of performance monitoring. These results bring additional evidence suggesting an unaltered ability to monitor perceptual performance on a trial by trial basis in schizophrenia."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1101/2022.10.19.22281249"

# Is this a featured publication? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["confidence","metacognition","EEG","ERP","ERN","schizophrenia"]

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = [""]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = "https://mpereira.netlify.com/publication/rouy2022/rouy2022.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

