+++
title="Human EEG reveals distinct neural correlates of power and precision grasping types"
date = 2018-11-01T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Iñaki Iturrate","Ricardo Chavarriaga", "Michael Pereira","Huaijian Zhang","Tiffany Corbet","Robert Leeb", "José del R. Millàn"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "In *Neuroimage*"
publication_short = "In *Neuroimage*"

# Abstract.
abstract = "Hand grasping is a sophisticated motor task that has received much attention by the neuroscientific community, which demonstrated how grasping activates a network involving parietal, pre-motor and motor cortices using fMRI, ECoG, LFPs and spiking activity. Yet, there is a need for a more precise spatio-temporal analysis as it is still unclear how these brain activations over large cortical areas evolve at the sub-second level. In this study, we recorded ten human participants (1 female) performing visually-guided, self-paced reaching and grasping with precision or power grips. Following the results, we demonstrate the existence of neural correlates of grasping from broadband EEG in self-paced conditions and show how neural correlates of precision and power grasps differentially evolve as grasps unfold. 100 ms before the grasp is secured, bilateral parietal regions showed increasingly differential patterns. Afterwards, sustained differences between both grasps occurred over the bilateral motor and parietal regions, and medial pre-frontal cortex. Furthermore, these differences were sufficiently discriminable to allow single-trial decoding with 70% decoding performance. Functional connectivity revealed differences at the network level between grasps in fronto-parietal networks, in terms of upper-alpha cortical oscillatory power with a strong involvement of ipsilateral hemisphere. Our results supported the existence of fronto-parietal recurrent feedback loops, with stronger interactions for precision grips due to the finer motor control required for this grasping type."

# Summary. An optional shortened abstract.
summary = ""

# Digital Object Identifier (DOI)
doi = "https://doi.org/10.1016/j.neuroimage.2018.07.055"

# Is this a featured publication? (true/false)
featured = false

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["EEG","grasping","BCI"]
# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = ["bci"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Links (optional).
url_pdf = ""
url_preprint = "iturrate2018.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

