+++
# Project title.
title = "Action monitoring"

# Date this page was created.
date = 2020-07-01T00:00:00

# Project summary to display on homepage.
summary = "How the brain monitors the quality of ongoing movements"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["visuomotor","metacognition","confidence","awareness"]

# Optional external URL for project (replaces project detail page).
#external_link = "http://example.org"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = "Photo by Toa Heftiba on Unsplash"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
#  focal_point = "Smart"
+++
