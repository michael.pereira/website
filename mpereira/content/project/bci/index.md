+++
# Project title.
title = "Closed-loop stimulation and feedback"

# Date this page was created.
date = 2020-07-01T00:00:00

# Project summary to display on homepage.
summary = "Providing feedback/adjusting stimulation based on ongoing brain activity"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["closed-loop","brain-computer interface","brain-machine interface","stimulation","machine-learning"]

# Optional external URL for project (replaces project detail page).
# external_link = "http://example.org"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
#  focal_point = "Smart"
+++
