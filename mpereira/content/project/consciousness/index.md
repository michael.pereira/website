+++
# Project title.
title = "Perceptual consciousness"

# Date this page was created.
date = 2021-07-01T00:00:00

# Project summary to display on homepage.
summary = "Is perceptual experience driven by an evidence accumulation process?  "

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["consciousness","metacognition","confidence","neurons","evidence","accumulation"]

# Optional external URL for project (replaces project detail page).
#external_link = "http://example.org"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
#  focal_point = "Smart"
+++
